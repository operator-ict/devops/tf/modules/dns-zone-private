variable "name" {
  type = string
}

variable "location" {
  type = string
}

variable "vnet_ids" {
  type = list(string)
}
variable "resource_group_name" {
  type = string
}
