resource "azurerm_private_dns_zone" "pdz" {
  name                = var.name
  resource_group_name = var.resource_group_name
}

# Join Spoke to Hub
resource "azurerm_private_dns_zone_virtual_network_link" "vnetLinks" {
  for_each = toset(var.vnet_ids)

  name                  = element(split("/", each.value), length(split("/", each.value)) - 1)
  virtual_network_id    = each.value
  private_dns_zone_name = azurerm_private_dns_zone.pdz.name
  resource_group_name   = var.resource_group_name
}
